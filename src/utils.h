#ifndef UTILS_H
#define UTILS_H

#include <string>

struct Aluno
{
    int codigo;
    std::string nome;
    float notas[2];
};
void autenticar();
float askNumero(std::string prompt_msg="");
int askCodigo();
std::string askNome();
float askNota();
float *askNotas();
void salvarAluno(Aluno alunos[], int alunos_size, int pos, bool edit_mode=false);
void listarAlunos(Aluno alunos[], int alunos_size);
int findAlunoPosByCodigo(Aluno alunos[], int alunos_size);
int getAlunoPosByCodigo(Aluno alunos[], int alunos_size, int codigo);
void editarAluno(Aluno alunos[], int alunos_count);
void excluirAluno(Aluno alunos[], int *alunos_count, int pos);
void pressAnyKey();

#endif
