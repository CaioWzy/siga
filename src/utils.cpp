#include <iostream>
#include <string>
#include <limits>
#include "utils.h"

using namespace std;

struct Usuario
{
    string usuario;
    string senha;
} usuario = {"admin", "admin"};

void autenticar()
{
    string usr, pwd;
    cout << "Usuário: ";
    cin >> usr;
    cout << "Senha: ";
    cin >> pwd;
    if (usuario.usuario != usr || usuario.senha != pwd)
    {
        cout << "Usuário e/ou senha errados, tente novamente!";
        exit(0);
    }
    cout << "Usuário autenticado com sucesso!" << endl;
}

float askNumero(string prompt_msg)
{
    float numero;
    if (prompt_msg.size())
    {
        cout << prompt_msg << " ";
    }
    cin >> numero;
    if (cin.fail())
    {
        cin.clear();
        cin.ignore(numeric_limits<streamsize>::max(), '\n');
        cout << "> " << "Você deve digitar *SOMENTE* caracteres numéricos." << endl;
        return askNumero(prompt_msg);
    }
    return numero;
}

int askCodigo()
{
    string prompt_msg = "Digite o código do aluno:";
    float codigo = askNumero(prompt_msg);
    if (codigo <= 0)
    {
        cout << "> O código do aluno deve ser um número maior que zero." << endl;
        return askCodigo();
    }
    return static_cast<int> (codigo);
}

string askNome()
{
    string nome;
    cout << "Digite o nome do aluno: ";
    cin >> nome;
    if (nome.size() < 4)
    {
        cout << "> O nome deve conter no mínimo quatro caracteres." << endl;
        return askNome();
    }
    return nome;
}

float askNota()
{
    string prompt_msg = "Digite a nota do aluno:";
    float nota = askNumero(prompt_msg);
    if (nota < 0 || nota > 10)
    {
        cout << "> A nota do aluno deve ser um número de 0 até 10." << endl;
        return askNota();
    }
    return nota;
}

float *askNotas()
{
    static float notas[2];
    for(int i = 0; i < 2; i++)
    {
        cout << i + 1 << "ª nota | ";
        notas[i] = askNota();
    }
    return notas;
}

void salvarAluno(Aluno alunos[], int alunos_size, int pos, bool edit_mode)
{
    int codigo, aluno_pos;
    while (true)
    {
        codigo = askCodigo();
        aluno_pos = getAlunoPosByCodigo(alunos, alunos_size, codigo);
        if (edit_mode && pos == aluno_pos)
        {
            break;
        }
        if (aluno_pos >= 0)
        {
            if (!edit_mode)
            {
                cout << "> Ops! Este código já existe, tente outro." << endl;
            }
            else
            {
                cout << "> Você deve usar o mesmo código do aluno ou outro ainda não utilizado." << endl;
            }
            continue;
        }
        break;
    }
    alunos[pos].codigo = codigo;
    alunos[pos].nome = askNome();
    float *notas = askNotas();
    for(int i = 0; i < 2; i++)
    {
        alunos[pos].notas[i] = notas[i];
    }
}

void excluirAluno(Aluno alunos[], int *alunos_size, int pos)
{
    if (*alunos_size == pos)
    {
        alunos[*alunos_size - 1].codigo = -1;
        *alunos_size -= 1;
        return;
    }
    alunos[pos] = alunos[*alunos_size - 1];
    alunos[*alunos_size - 1].codigo = -1;
    *alunos_size -= 1;
}

void listarAlunos(Aluno alunos[], int alunos_size)
{
    int cnt = 0;
    cout << "CÓDIGO \t NOME \t NOTAS" << endl;
    cout << endl;
    for(int i = 0; i < alunos_size; i++)
    {
        if (alunos[i].codigo == 0)
        {
            break;
        }
        if (alunos[i].codigo < 0)
        {
            continue;
        }
        cout << alunos[i].codigo << "\t";
        cout << alunos[i].nome << "\t";
        for(int j = 0; j < 2; j++)
        {
            if (j > 0)
            {
                cout << ", ";
            }
            cout << alunos[i].notas[j];
        }
        cout << endl;
        cnt++;
    }
    if (cnt != 0)
    {
        cout << endl;
    }
    cout << "Foram encontrados " << cnt << " registros." << endl;
}

int getAlunoPosByCodigo(Aluno alunos[], int alunos_size, int codigo)
{
    for(int i = 0; i < alunos_size; i++)
    {
        if (alunos[i].codigo == 0)
        {
            break;
        }
        if(alunos[i].codigo == codigo)
        {
            return i;
        }
    }
    return -1;
}

int findAlunoPosByCodigo(Aluno alunos[], int alunos_size)
{
    int pos = getAlunoPosByCodigo(alunos, alunos_size, askCodigo());
    if (pos < 0)
    {
        cout << "> Nenhum aluno com este código foi encontrado." << endl;
        return -1;
    }
    return pos;
}

void pressAnyKey()
{
    getchar();
    cout << "* Pressione ENTER para voltar ao menu principal." << endl;
    getchar();
}
