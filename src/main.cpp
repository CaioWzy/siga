#include <iostream>
#include <string>
#include "utils.h"

using namespace std;

Aluno alunos[3];
int alunos_size = sizeof(alunos) / sizeof(*alunos);
int alunos_count = 0;

void Inicio();

int main()
{
    autenticar();
    while(true)
    {
        Inicio();
    }
}

void Inicio()
{
    cout << string( 100, '\n' );
    cout << R"(
                    .-~~~-.
                  /        }        Bem-vindo ao SIGA. Para sair digite 0.
                 /      .-~
                |        }              1) Exibir
    ___\.~~-.-~|     .-~_               2) Cadastrar
        { O  |  ` .-~.   ; ~-.__        3) Alterar
         ~--~/-|_\|  :   : .-~          4) Excluir
            /  |  \~ - - ~
           /   |   \                )";

    int op = static_cast<int> (askNumero("Opções:"));
    cout << endl;
    switch(op){

        case 0:
            cout << "Bye." << endl;
            exit(0);
            break;

        case 1:
            listarAlunos(alunos, alunos_size);
            break;

        case 2:
            if (alunos_count >= alunos_size) {
                cout << "> Não há mais espaço para um novo cadastro. "
                        "Exclua alguns alunos para poder adicionar novamente." << endl;
                break;
            }
            salvarAluno(alunos, alunos_size, alunos_count);
            alunos_count++;
            cout << "> O aluno foi cadastrado com sucesso." << endl;
            break;

        case 3:
            {
                int pos = findAlunoPosByCodigo(alunos, alunos_size);
                if (pos >= 0)
                {
                    cout << "> Aluno encontrado. "
                            "Agora você pode digitar os novos dados para o registro." << endl;
                    salvarAluno(alunos, alunos_size, pos, true);
                }
            }
            break;

        case 4:
            {
                int pos = findAlunoPosByCodigo(alunos, alunos_size);
                if (pos >= 0)
                {
                    excluirAluno(alunos, &alunos_count, pos);
                    cout << "> Aluno excluído." << endl;
                }
            }
            break;
    }
    pressAnyKey();
}

